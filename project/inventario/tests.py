import datetime
from django.test import TestCase
from django.utils import timezone
from .models import bodega


class bodegaModelTests(TestCase):
	def test_dimension_equivocada(self):
		"""
		retorna falso, cuando la dimension es inferior a 5.
		"""
		bodegareducida = bodega(bode_nombre = "Aurora",
			bode_dimension = 4,
			bode_ubicacion = "Nothing",
			bode_estado=1,
			bode_estado_logico=1
			)
		self.assertIs(bodegareducida.getLimitdimesion(), False)
