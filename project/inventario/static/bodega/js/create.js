$(document).ready(function () {	
	$('#insert_bodega').click(function () {	
		bode_nombre = $('#nombre_bodega').val();
		bode_dimension = $('#dimension_bodega').val();
		bode_ubicacion = $('#ubicacion_bodega').val();
		bode_capacidad = $('#capacidad_bodega').val();
		bode_total_prod =$('#total_prod_bodega').val();
		$.ajax({
			url:'guardar_bodega',
			type: 'GET',
			data:{
				'bode_nombre': bode_nombre,
				'bode_dimension': bode_dimension,
	          	'bode_ubicacion': bode_ubicacion,
	          	'bode_capacidad': bode_capacidad,
	          	'bode_total_prod': bode_total_prod, 
			},	
			dataType: 'json',
			success: function (data) {
				if (data.out_insert==1) {
	            	alert("Informacion guardada exitosamente");	            
	          	}else{
	          		alert("Error: La informacion no ha sido guardada");	            	          	
	          	}				
	          	document.location.href = "indexbodega";
			},
		});
	});	
});
	