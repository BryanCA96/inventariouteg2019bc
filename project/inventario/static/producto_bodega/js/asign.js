$(document).ready(function () {	
	$('#asignar_producto_bodega').click(function () {	
		bodega_id = $('#id_bodega').val();
		producto_id = $('#id_producto').val();
		pbod_cantidad_productos = $('#cantidad_productos_productobodega').val();
		$.ajax({
			url:'asignar_producto_bodega',
			type: 'GET',
			data:{
				'bodega_id': bodega_id,
				'producto_id': producto_id,
	          	'pbod_cantidad_productos': pbod_cantidad_productos,	          	
			},	
			dataType: 'json',
			success: function (data) {
				if (data.out_asignar==1) {
	            	alert("Producto asignado a bodega");	            
	          	}else{
	          		alert("Error: La informacion no ha sido guardada");	            	          	
	          	}				
	          	document.location.href = "../indexproductobodega";
			},
		});



	});
});