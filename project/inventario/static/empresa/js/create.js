$(document).ready(function () {	
	$('#insert_empresa').click(function () {	
		emp_nombre_comercial = $('#nombre_comercial_empresa').val();
		emp_razon_social = $('#razon_social_empresa').val();
		
		$.ajax({
			url:'guardar_empresa',
			type: 'GET',
			data:{
				'emp_nombre_comercial': emp_nombre_comercial,
				'emp_razon_social': emp_razon_social,
	          	
			},	
			dataType: 'json',
			success: function (data) {
				if (data.out_insert==1) {
	            	alert("Informacion guardada exitosamente");	            
	          	}else{
	          		alert("Error: La informacion no ha sido guardada");	            	          	
	          	}				
	          	document.location.href = "indexempresa";
			},
		});
	});	
});