$(document).ready(function () {	
	$('#update_empresa').click(function (evt) {
		emp_id = $('#id_empresa').val();
		emp_nombre_comercial = $('#nombre_comercial_empresa').val();
		emp_razon_social = $('#razon_social_empresa').val();
		
		$.ajax({
			url: '../actualizar_empresa',
			type:"GET",
			data: {
			  'emp_id': emp_id,
			  'emp_nombre_comercial': emp_nombre_comercial,
			  'emp_razon_social': emp_razon_social,
		  
			},
			dataType: 'json',
			success: function (data) {
				mensaje="";
			  if (data.out_update==1) {
				mensaje="Informacion guardada exitosamente";	          
			  }else{
				mensaje="Error: La informacion no ha sido guardada";	            	          	
			  }
			  	
				document.location.href = "../indexempresa";
			}
		});
});
});