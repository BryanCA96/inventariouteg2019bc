$(document).ready(function () {	
	$('#insert_persona').click(function () {	
		per_nombre = $('#nombre_persona').val();
		per_apellido = $('#apellido_persona').val();
		per_fecha_nacimiento = $('#fecha_nacimiento_persona').val();
		
		$.ajax({
			url:'guardar_persona',
			type: 'GET',
			data:{
				'per_nombre': per_nombre,
				'per_apellido': per_apellido,
	          	'per_fecha_nacimiento': per_fecha_nacimiento,
	          	
			},	
			dataType: 'json',
			success: function (data) {
				if (data.out_insert==1) {
	            	alert("Informacion guardada exitosamente");	            
	          	}else{
	          		alert("Error: La informacion no ha sido guardada");	            	          	
	          	}				
	          	document.location.href = "indexpersona";
			},
		});
	});	
});
	