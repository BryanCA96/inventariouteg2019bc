$(document).ready(function () {	
	$('#insert_producto').click(function () {	
		prod_nombre = $('#nombre_producto').val();
		prod_peso = $('#peso_producto').val();
		prod_tamanio = $('#tamanio_producto').val();
		prod_precio = $('#precio_producto').val();
		$.ajax({
			url:'guardar_producto',
			type: 'GET',
			data:{
				'prod_nombre': prod_nombre,
				'prod_peso': prod_peso,
	          	'prod_tamanio': prod_tamanio,
	          	'prod_precio': prod_precio,
			},	
			dataType: 'json',
			success: function (data) {
				if (data.out_insert==1) {
	            	alert("Informacion guardada exitosamente");	            
	          	}else{
	          		alert("Error: La informacion no ha sido guardada");	            	          	
	          	}				
	          	document.location.href = "indexproducto";
			},
		});
	});	
});
	