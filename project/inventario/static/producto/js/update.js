$(document).ready(function () {	
	$('#update_producto').click(function (evt) {
		prod_id = $('#id_producto').val();
		prod_nombre = $('#nombre_producto').val();
		prod_peso = $('#peso_producto').val();
		prod_tamanio = $('#tamanio_producto').val();
		prod_precio = $('#precio_producto').val();
		$.ajax({
			url: '../actualizar_producto',
			type:"GET",
			data: {
			  'prod_id': prod_id,
			  'prod_nombre': prod_nombre,
			  'prod_peso': prod_peso,
			  'prod_tamanio': prod_tamanio,
			  'prod_precio': prod_precio,
			},
			dataType: 'json',
			success: function (data) {
				mensaje="";
			  if (data.out_update==1) {
				mensaje="Informacion guardada exitosamente";	          
			  }else{
				mensaje="Error: La informacion no ha sido guardada";	            	          	
			  }
			  	
				document.location.href = "../indexproducto";
			}
		});
});
});