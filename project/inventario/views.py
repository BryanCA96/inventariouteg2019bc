from django.shortcuts import render
from django.http import HttpResponse
from .models import bodega
from .models import producto
from .models import empresa
from .models import persona
from .models import productoBodega
from django.template import loader
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
import datetime

def indexPrincipal(request):
	return render(request, 'index_principal/index_principal.html')

def indexBodega(request):
	bodegas = bodega.objects.all()
	context = {
		'bodegas': bodegas,
	}
	return render(request, 'bodega/index.html', context)

def asignproductobodega(request):
	bodegas = bodega.objects.all()
	productos = producto.objects.all()
	context = {
		'bodegas': bodegas,
		'productos': productos,
	}
	return render(request, 'producto_bodega/asign.html', context)

def indexProducto(request):
	productos = producto.objects.all()
	context = {
		'productos': productos,
	}
	return render(request, 'producto/index.html', context)

def indexEmpresa(request):
	empresas = empresa.objects.all()
	context = {
		'empresas': empresas,
	}
	return render(request, 'empresa/index.html', context)

def indexPersona(request):
	personas = persona.objects.all()
	context = {
		'personas': personas,
	}
	return render(request, 'persona/index.html', context)

def createBodega(request):
	return render(request, 'bodega/create.html'	)

def createProducto(request):
	return render(request, 'producto/create.html'	)

def createEmpresa(request):
	return render(request, 'empresa/create.html'	)

def createPersona(request):
	return render(request, 'persona/create.html'	)

def updateBodega(request,bodega_id):
	bodega_obj = get_object_or_404(bodega, pk=bodega_id)
	template = loader.get_template('bodega/update.html')
	context = {
	    'bodega': bodega_obj,
	}
	return HttpResponse(template.render(context, request))

def updateProducto(request,producto_id):
	producto_obj = get_object_or_404(producto, pk=producto_id)
	template = loader.get_template('producto/update.html')
	context = {
	    'producto': producto_obj,
	}
	return HttpResponse(template.render(context, request))

def updateEmpresa(request,empresa_id):
	empresa_obj = get_object_or_404(empresa, pk=empresa_id)
	template = loader.get_template('empresa/update.html')
	context = {
	    'empresa': empresa_obj,
	}
	return HttpResponse(template.render(context, request))

def updatePersona(request,persona_id):
	persona_obj = get_object_or_404(persona, pk=persona_id)
	template = loader.get_template('persona/update.html')
	context = {
	    'persona': persona_obj,
	}
	return HttpResponse(template.render(context, request))

def guardar_producto(request):
	prod_nombre = request.GET.get('prod_nombre', None)
	prod_peso = request.GET.get('prod_peso', None)	
	prod_tamanio = request.GET.get('prod_tamanio', None)
	prod_precio = request.GET.get('prod_precio', None)	
		
	out=0
	hoy = datetime.datetime.now()
	try:
		prod_obj = producto(prod_nombre=prod_nombre, prod_estado=1, prod_estado_logico=1)
		prod_obj.prod_peso=prod_peso
		prod_obj.prod_tamanio=prod_tamanio
		prod_obj.prod_precio=prod_precio
		prod_obj.prod_usuario_ingreso=1
		prod_obj.prod_fecha_ingreso=hoy.strftime("%Y-%m-%d %H:%M")			
		prod_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_insert': out
	}
	return JsonResponse(data)

def indexproductobodega(request):
	probod = productoBodega.objects.raw("SELECT inventario_productobodega.id,inventario_producto.id as producto_id,prod_nombre,bode_nombre,pbod_fecha_asignacion FROM inventario_productobodega  inner join inventario_producto on inventario_producto.id =  inventario_productobodega.id inner join inventario_bodega on inventario_bodega.id = inventario_productobodega.id;")
	context = {
		'probods': probod,
	}
	return render(request, 'producto_bodega/index.html', context)

def asignar_producto_bodega(request):
	bodega_id = request.GET.get('bodega_id', None)
	producto_id = request.GET.get('producto_id', None)	
	pbod_cantidad_productos = request.GET.get('pbod_cantidad_productos', None)	
	out=0
	hoy = datetime.datetime.now()
	try:
		bode_obj = bodega.objects.get(id=bodega_id)
		prod_obj = producto.objects.get(id=producto_id)
		pbod_obj = productoBodega(bode=bode_obj, prod=prod_obj, pbod_estado=1, pbod_estado_logico=1)
		pbod_obj.pbod_cantidad_productos = pbod_cantidad_productos
		pbod_obj.pbod_usuario_ingreso=1
		pbod_obj.pbod_fecha_asignacion=hoy.strftime("%Y-%m-%d %H:%M")
		pbod_obj.pbod_fecha_ingreso=hoy.strftime("%Y-%m-%d %H:%M")			
		pbod_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_asignar': out
	}
	return JsonResponse(data)

def guardar_persona(request):
	per_nombre = request.GET.get('per_nombre', None)
	per_apellido = request.GET.get('per_apellido', None)	
	per_fecha_nacimiento = request.GET.get('per_fecha_nacimiento', None)	
		
	out=0
	hoy = datetime.datetime.now()
	try:
		per_obj = persona(per_nombre=per_nombre, per_estado=1, per_estado_logico=1)
		per_obj.per_apellido=per_apellido
		per_obj.per_fecha_nacimiento=per_fecha_nacimiento
		per_obj.per_usuario_ingreso=1
		per_obj.per_fecha_ingreso=hoy.strftime("%Y-%m-%d %H:%M")			
		per_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_insert': out
	}
	return JsonResponse(data)

def guardar_empresa(request):
	emp_nombre_comercial = request.GET.get('emp_nombre_comercial', None)
	emp_razon_social = request.GET.get('emp_razon_social', None)	
		
	out=0
	hoy = datetime.datetime.now()
	try:
		emp_obj = empresa(emp_nombre_comercial=emp_nombre_comercial, emp_estado=1, emp_estado_logico=1)
		emp_obj.emp_razon_social=emp_razon_social
		emp_obj.emp_usuario_ingreso=1
		emp_obj.emp_fecha_ingreso=hoy.strftime("%Y-%m-%d %H:%M")			
		emp_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_insert': out
	}
	return JsonResponse(data)

def guardar_bodega(request):
	bode_nombre = request.GET.get('bode_nombre', None)
	bode_dimension = request.GET.get('bode_dimension', None)	
	bode_ubicacion = request.GET.get('bode_ubicacion', None)	
	bode_capacidad = request.GET.get('bode_capacidad', None)
	bode_total_prod = request.GET.get('bode_total_prod', None)	
	out=0
	hoy = datetime.datetime.now()
	try:
		bode_obj = bodega(bode_nombre=bode_nombre, bode_ubicacion=bode_ubicacion, bode_estado=1, bode_estado_logico=1)
		bode_obj.bode_dimension=bode_dimension
		bode_obj.bode_capacidad=bode_capacidad
		bode_obj.bode_total_prod=bode_total_prod
		bode_obj.bode_usuario_ingreso=1
		bode_obj.bode_fecha_ingreso=hoy.strftime("%Y-%m-%d %H:%M")			
		bode_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_insert': out
	}
	return JsonResponse(data)

def actualizar_bodega(request):
	bode_id = request.GET.get('bode_id', None)
	bode_nombre = request.GET.get('bode_nombre', None)
	bode_dimension = request.GET.get('bode_dimension', None)	
	bode_ubicacion = request.GET.get('bode_ubicacion', None)	
	bode_capacidad = request.GET.get('bode_capacidad', None)
	bode_total_prod = request.GET.get('bode_total_prod', None)	
	out=0
	hoy = datetime.datetime.now()
	try:
		bode_obj = bodega.objects.get(id=bode_id)
		bode_obj.bode_nombre=bode_nombre
		bode_obj.bode_dimension=bode_dimension	
		bode_obj.bode_capacidad=bode_capacidad
		bode_obj.bode_ubicacion=bode_ubicacion
		bode_obj.bode_total_prod=bode_total_prod
		bode_obj.bode_fecha_modificacion=hoy.strftime("%Y-%m-%d %H:%M")			
		bode_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_update': out
	}
	return JsonResponse(data)

def actualizar_producto(request):
	prod_id = request.GET.get('prod_id', None)
	prod_nombre = request.GET.get('prod_nombre', None)
	prod_peso = request.GET.get('prod_peso', None)	
	prod_tamanio = request.GET.get('prod_tamanio', None)
	prod_precio = request.GET.get('prod_precio', None)
	out=0
	hoy = datetime.datetime.now()
	try:
		prod_obj = producto.objects.get(id=prod_id)
		prod_obj.prod_nombre=prod_nombre
		prod_obj.prod_peso=prod_peso
		prod_obj.prod_tamanio=prod_tamanio
		prod_obj.prod_precio=prod_precio
		prod_obj.prod_fecha_modificacion=hoy.strftime("%Y-%m-%d %H:%M")			
		prod_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_update': out
	}
	return JsonResponse(data)

def actualizar_persona(request):
	per_id = request.GET.get('per_id', None)
	per_nombre = request.GET.get('per_nombre', None)
	per_apellido = request.GET.get('per_apellido', None)	
	per_fecha_nacimiento = request.GET.get('per_fecha_nacimiento', None)
	out=0
	hoy = datetime.datetime.now()
	try:
		per_obj = persona.objects.get(id=per_id)
		per_obj.per_nombre=per_nombre
		per_obj.per_apellido=per_apellido
		per_obj.per_fecha_nacimiento=per_fecha_nacimiento
		per_obj.per_fecha_modificacion=hoy.strftime("%Y-%m-%d %H:%M")			
		per_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_update': out
	}
	return JsonResponse(data)

def actualizar_empresa(request):
	emp_id = request.GET.get('emp_id', None)
	emp_nombre_comercial = request.GET.get('emp_nombre_comercial', None)
	emp_razon_social = request.GET.get('emp_razon_social', None)	
	out=0
	hoy = datetime.datetime.now()
	try:
		emp_obj = empresa.objects.get(id=emp_id)
		emp_obj.emp_nombre_comercial=emp_nombre_comercial
		emp_obj.emp_razon_social=emp_razon_social
		emp_obj.emp_fecha_modificacion=hoy.strftime("%Y-%m-%d %H:%M")			
		emp_obj.save()	
		out=1
	except :
		out=0
	data = {
		'out_update': out
	}
	return JsonResponse(data)