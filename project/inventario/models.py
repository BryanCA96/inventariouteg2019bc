from django.db import models

# Create your models here.
class bodega(models.Model):
	bode_nombre = models.CharField(max_length=200)
	bode_dimension = models.FloatField(null=True)
	bode_ubicacion = models.CharField(max_length=300)
	bode_capacidad = models.FloatField(null=True)
	bode_total_prod = models.FloatField(null=True)
	bode_estado  = models.CharField(max_length=1)
	bode_usuario_ingreso = models.IntegerField(null=True)
	bode_usuario_modifica  = models.IntegerField(null=True)
	bode_fecha_ingreso = models.DateTimeField(null=True)
	bode_fecha_modificacion = models.DateTimeField(null=True)
	bode_estado_logico  = models.CharField(max_length=1)
	def __str__(self):
		return self.bode_nombre
	def getLimitdimesion(self):
		if(self.bode_dimension>=5):
			return True
		else:
			return False

class producto(models.Model):

	prod_nombre = models.CharField(max_length=200)
	prod_peso = models.FloatField(null=True)
	prod_tamanio = models.FloatField(null=True)
	prod_precio = models.FloatField(null=True)
	prod_estado  = models.CharField(max_length=1)
	prod_usuario_ingreso = models.IntegerField(null=True)
	prod_usuario_modifica  = models.IntegerField(null=True)
	prod_fecha_ingreso = models.DateTimeField(null=True)
	prod_fecha_modificacion = models.DateTimeField(null=True)
	prod_estado_logico  = models.CharField(max_length=1)
	def __str__(self):
		return self.prod_nombre
	def getLimitdimesion(self):
		if(self.prod_tamanio>=5):
			return True
		else:
			return False

class productoBodega(models.Model):

	bode = models.ForeignKey(bodega, on_delete=models.CASCADE)
	prod = models.ForeignKey(producto, on_delete=models.CASCADE)
	pbod_fecha_asignacion =models.DateTimeField(null=True)
	pbod_cantidad_productos=models.FloatField(null=True)
	pbod_estado  = models.CharField(max_length=1)
	pbod_usuario_ingreso = models.IntegerField(null=True)
	pbod_usuario_modifica  = models.IntegerField(null=True)
	pbod_fecha_ingreso = models.DateTimeField(null=True)
	pbod_fecha_modificacion = models.DateTimeField(null=True)
	pbod_estado_logico  = models.CharField(max_length=1)

class empresa(models.Model):

	emp_nombre_comercial = models.CharField(max_length=200)
	emp_razon_social = models.CharField(max_length=200)
	emp_estado  = models.CharField(max_length=1)
	emp_usuario_ingreso = models.IntegerField(null=True)
	emp_usuario_modifica  = models.IntegerField(null=True)
	emp_fecha_ingreso = models.DateTimeField(null=True)
	emp_fecha_modificacion = models.DateTimeField(null=True)
	emp_estado_logico  = models.CharField(max_length=1)

class persona(models.Model):
	
	per_nombre  = models.CharField(max_length=200)
	per_apellido  = models.CharField(max_length=200)
	per_fecha_nacimiento = models.DateTimeField(null=True)
	per_estado  = models.CharField(max_length=1)
	per_usuario_ingreso = models.IntegerField(null=True)
	per_usuario_modifica  = models.IntegerField(null=True)
	per_fecha_ingreso = models.DateTimeField(null=True)
	per_fecha_modificacion = models.DateTimeField(null=True)
	per_estado_logico  = models.CharField(max_length=1)
class proveedor(models.Model):

	per = models.ForeignKey(persona, on_delete=models.CASCADE)
	emp = models.ForeignKey(empresa, on_delete=models.CASCADE)
	prov_fecha_asignacion = models.DateTimeField(null=True)
	prov_estado  = models.CharField(max_length=1)
	prov_usuario_ingreso = models.IntegerField(null=True)
	prov_usuario_modifica  = models.IntegerField(null=True)
	prov_fecha_ingreso = models.DateTimeField(null=True)
	prov_fecha_modificacion = models.DateTimeField(null=True)
	prov_estado_logico  = models.CharField(max_length=1)