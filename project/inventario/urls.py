from django.urls import path
from . import views
from django.conf.urls import url
# esto es un comentario
urlpatterns = [
	path('indexprincipal', views.indexPrincipal, name='indexprincipal'),
	path('asignproductobodega', views.asignproductobodega, name='asignproductobodega'),
	path('indexproductobodega', views.indexproductobodega, name='indexproductobodega'),

	url(r'^asignar_producto_bodega$', views.asignar_producto_bodega, name='asignar_producto_bodega'),

	path('indexbodega', views.indexBodega, name='indexbodega'),
	path('createbodega', views.createBodega, name='createbodega'),
	path('updatebodega', views.updateBodega, name='updatebodega'),
	url(r'^guardar_bodega$', views.guardar_bodega, name='guardar_bodega'),
	url(r'^actualizar_bodega$', views.actualizar_bodega, name='actualizar_bodega'),
	path('<int:bodega_id>/updatebodega', views.updateBodega, name='updatebodega'),

	path('indexproducto', views.indexProducto, name='indexproducto'),
	path('createproducto', views.createProducto, name='createproducto'),
	path('updateproducto', views.updateProducto, name='updateproducto'), 
	url(r'^guardar_producto$', views.guardar_producto, name='guardar_producto'),
	url(r'^actualizar_producto$', views.actualizar_producto, name='actualizar_producto'),
	path('<int:producto_id>/updateproducto', views.updateProducto, name='updateproducto'),

	path('indexempresa', views.indexEmpresa, name='indexempresa'),
	path('createempresa', views.createEmpresa, name='createempresa'),
	path('updateempresa', views.updateEmpresa, name='updateempresa'), 
	url(r'^guardar_empresa$', views.guardar_empresa, name='guardar_empresa'),
	url(r'^actualizar_empresa$', views.actualizar_empresa, name='actualizar_empresa'),
	path('<int:empresa_id>/updateempresa', views.updateEmpresa, name='updateempresa'),

	path('indexpersona', views.indexPersona, name='indexpersona'),
	path('createpersona', views.createPersona, name='createpersona'),
	path('updatepersona', views.updatePersona, name='updatepersona'), 
	url(r'^guardar_persona$', views.guardar_persona, name='guardar_persona'),
	url(r'^actualizar_persona$', views.actualizar_persona, name='actualizar_persona'),
	path('<int:persona_id>/updatepersona', views.updatePersona, name='updatepersona'),

]
